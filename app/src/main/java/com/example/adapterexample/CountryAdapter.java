package com.example.adapterexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryAdapter extends BaseAdapter {
    Context c ;
    ArrayList<Country> data;
    int resource;
    String country_name_str;
    String country_capital_str;

    public CountryAdapter(Context c, ArrayList<Country> data, int resource) {
        this.c = c;
        this.data = data;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null) {
          v =  LayoutInflater.from(c).inflate(resource,null,false);
        }
        TextView countryname = v.findViewById(R.id.country_name);
        TextView countrycapital = v.findViewById(R.id.capital);
        ImageView countryimage = v.findViewById(R.id.img);
        Country country = (Country) getItem(position);
        int country_flag = country.getImg();
        country_name_str  = country.getName();
        country_capital_str = country.getCapital();

        countryname.setText(country_name_str);
        countrycapital.setText(country_capital_str);
        countryimage.setImageResource(country_flag);


            return v;
    }
}

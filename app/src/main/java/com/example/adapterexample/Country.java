package com.example.adapterexample;

import java.io.Serializable;

public class Country implements Serializable {

    String name,capital;
    int img;

    public Country(String name, String capital, int img) {
        this.name = name;
        this.capital = capital;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}

package com.example.adapterexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ListViewExample extends AppCompatActivity {
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_example);
        lv = findViewById(R.id.Li01);


        Country palestine = new Country("palestine","jerusalem",R.drawable.palestine);
        Country sudia = new Country("saudi","Read",R.drawable.su);
        Country turkey = new Country("tucky","Angara",R.drawable.tr);


      ArrayList <Country> names = new ArrayList<>();
      names.add(palestine);
      names.add(sudia);
      names.add(turkey);

      CountryAdapter myAdapter = new CountryAdapter(this,names,R.layout.country_row);
      myAdapter.notifyDataSetChanged();
        lv.setAdapter(myAdapter);



    }
}
